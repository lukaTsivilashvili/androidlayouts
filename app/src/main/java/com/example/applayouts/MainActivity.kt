package com.example.applayouts

import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import com.example.applayouts.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity(), View.OnClickListener {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        val btnList = mutableListOf(
            binding.btnRed,
            binding.btnOrange,
            binding.btnYellow,
            binding.btnGreen,
            binding.btnBlue,
            binding.btnDarkRed,
            binding.btnDarkOrange,
            binding.btnDarkYellow,
            binding.btnDarkGreen,
            binding.btnNewActivity
        )

        for (btn in btnList){
            btn.setOnClickListener(this)
        }

    }

    override fun onClick(clickedView: View?) {
        if (clickedView is Button) {
            when (clickedView.id) {

                R.id.btnRed -> {
                    clickedView.setBackgroundColor(Color.parseColor("#FF0000"))
                    clickedView.text = "წითელი"
                }
                R.id.btnOrange -> {
                    clickedView.setBackgroundColor(Color.parseColor("#ffa500"))
                    clickedView.text = "სტაფილოსფერი"
                }
                R.id.btnYellow -> {
                    clickedView.setBackgroundColor(Color.parseColor("#ffff00"))
                    clickedView.text = "ყვითელი"
                }
                R.id.btnGreen -> {
                    clickedView.setBackgroundColor(Color.parseColor("#32cd32"))
                    clickedView.text = "მწვანე"
                }
                R.id.btnBlue -> {
                    clickedView.setBackgroundColor(Color.parseColor("#1e90ff"))
                    clickedView.text = "ლურჯი"
                }
                R.id.btnDarkRed -> {
                    clickedView.setBackgroundColor(Color.parseColor("#8b0000"))
                    clickedView.text = "მუქი წითელი"
                }
                R.id.btnDarkOrange -> {
                    clickedView.setBackgroundColor(Color.parseColor("#cc7000"))
                    clickedView.text = "მუქი სტაფილოსფერი"
                }
                R.id.btnDarkYellow -> {
                    clickedView.setBackgroundColor(Color.parseColor("#b3b300"))
                    clickedView.text = "მუქი ყვითელი"
                }
                R.id.btnDarkGreen -> {
                    clickedView.setBackgroundColor(Color.parseColor("#008000"))
                    clickedView.text = "მუქი მწვანე"
                }
                R.id.btnNewActivity -> {
                    clickedView.setBackgroundColor(Color.parseColor("#00008b"))
                    val intent = Intent(this, MatrixConstraint::class.java)
                    startActivity(intent)
                }
            }
        }
    }

}