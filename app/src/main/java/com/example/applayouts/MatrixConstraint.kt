package com.example.applayouts

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import com.example.applayouts.databinding.ActivityMainBinding
import com.example.applayouts.databinding.ActivityMatrixConstraintBinding
import kotlin.random.Random

class MatrixConstraint : AppCompatActivity(), View.OnClickListener {

    private lateinit var binding: ActivityMatrixConstraintBinding

    val colors = listOf(
        "#FF0000",
        "#ffa500",
        "#ffff00",
        "#32cd32",
        "#1e90ff",
        "#8b0000",
        "#cc7000",
        "#b3b300",
        "#008000"
    )
    var randomColor = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMatrixConstraintBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)


        val btnList = mutableListOf(
            binding.button,
            binding.button2,
            binding.button3,
            binding.button4,
            binding.button5,
            binding.button6,
            binding.button7,
            binding.button8,
            binding.button9,
            binding.button10
        )

        for (btn in btnList) {
            btn.setOnClickListener(this)
        }

    }

    override fun onClick(clickedView: View?) {

        if (clickedView is Button) {
            when (clickedView.id) {
                R.id.button -> {
                    clickedView.text = "2"
                    randomColor = colors.get(Random.nextInt(colors.size))
                    clickedView.setBackgroundColor(Color.parseColor(randomColor))
                }
                R.id.button2 -> {
                    clickedView.text = "1"
                    randomColor = colors.get(Random.nextInt(colors.size))
                    clickedView.setBackgroundColor(Color.parseColor(randomColor))
                }
                R.id.button3 -> {
                    clickedView.text = "3"
                    randomColor = colors.get(Random.nextInt(colors.size))
                    clickedView.setBackgroundColor(Color.parseColor(randomColor))
                }
                R.id.button4 -> {
                    clickedView.text = "4"
                    randomColor = colors.get(Random.nextInt(colors.size))
                    clickedView.setBackgroundColor(Color.parseColor(randomColor))
                }
                R.id.button5 -> {
                    clickedView.text = "5"
                    randomColor = colors.get(Random.nextInt(colors.size))
                    clickedView.setBackgroundColor(Color.parseColor(randomColor))
                }
                R.id.button6 -> {
                    clickedView.text = "6"
                    randomColor = colors.get(Random.nextInt(colors.size))
                    clickedView.setBackgroundColor(Color.parseColor(randomColor))
                }
                R.id.button7 -> {
                    clickedView.text = "7"
                    randomColor = colors.get(Random.nextInt(colors.size))
                    clickedView.setBackgroundColor(Color.parseColor(randomColor))
                }
                R.id.button8 -> {
                    clickedView.text = "8"
                    randomColor = colors.get(Random.nextInt(colors.size))
                    clickedView.setBackgroundColor(Color.parseColor(randomColor))
                }
                R.id.button9 -> {
                    clickedView.text = "9"
                    randomColor = colors.get(Random.nextInt(colors.size))
                    clickedView.setBackgroundColor(Color.parseColor(randomColor))
                }
                R.id.button10 -> {
                    clickedView.text = "10"
                    randomColor = colors.get(Random.nextInt(colors.size))
                    clickedView.setBackgroundColor(Color.parseColor(randomColor))
                }
            }
        }

    }

}